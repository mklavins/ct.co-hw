#include <iostream>
#include <vector>
#include <iomanip>
#include <math.h>
#include <algorithm>
using namespace std;

class Friend {
/* class defines object "Friend" for each friend, that participated in vacation */
public:
   string name;            // friends name
   double expenses;        // total amount of money that friend has spent on vacation
   Friend(string name_){
      if(name_ == "") name = "unidentified friend";      // for cases, when you have a new friend, but you forgot his name
      else{
         name_[0] = toupper(name_[0]);
         for(unsigned int i = 1; i < name_.length(); i++){
            if(name_[i-1] == ' ') name_[i] = toupper(name_[i]);
            else name_[i]=tolower(name_[i]);
         }
         name = name_;
      }
      expenses = 0;
   };
   bool pay(double sum){
   // pay(amount) - increase expenses for a friend by 'amount'. Used in data input part.
      if (sum > 0) expenses += sum;
      else return 0;
      return 1;
   };

};
string convertToLower(string str);                    // convert all the characters in string str to lowercase
int findFriend(string name_, vector<Friend*>& f);     // return position of a friends  pointer in vector; -1 if friend has not been registered
void calculateTransactions(vector<Friend*>& f);       // calcualtes all the transactions necessary for all friends to be equal
bool compByExp(Friend* f1, Friend* f2){ return (f1->expenses < f2->expenses); }; // used to sort Friend objects in vector by their expenses (smaller -> bigger)

int main(){
   char ok;                      // used to control, wether user wants to add another friend / expense
   string f_name;                // friend name from input
   string service;               // service friend provided - from input
   double amount;                // amount of money friend spent on service
   vector<Friend*> friends;      //  vector friends will hold all the pointers to objects
   unsigned int friend_id = 0;   // counter used to assign ID values for friends (as indexes in vector)
   do{ // data input
      cout << "Name: ";
      getline(cin, f_name);
      cout << "Service: ";
      getline(cin, service);
      cout << "Amount: ";
      cin >> amount;
      // check, if friend has been registered before.
      friend_id = findFriend(f_name, friends);
      // [if]- riend has not been registered before so we create new element in vector and new Friend object
      if (friend_id == -1){
         if (amount > 0) {
            friends.push_back(new Friend(f_name));
            friends[friends.size()-1]->pay(amount);
         }
      }
      // [else]- friend has been registered before, so we increase amount of his/her expenses by amount
      else friends[friend_id]->pay(amount);
      cout << "Add another? (y/n)" << endl;
      cin >> ok;
      cin.ignore();
   }while (tolower(ok) == 'y'); // data input repeats as long as user is willing to continue

   calculateTransactions(friends);  // transaction calculation and printing the results
   return 0;
};

string convertToLower(string str){
// convert all the characters in string str to lowercase
   for(unsigned int i = 0; i < str.length(); i++)
      str[i]=tolower(str[i]);
   return str;
};
int findFriend(string name_, vector<Friend*>& f){
// return position of a friends  pointer in vector; -1 if friend has not been registered
   if (name_ == "") name_ == "unidentified friend";
   for (unsigned int i = 0; i < f.size(); i++)
      if (convertToLower(name_) == convertToLower(f[i]->name)) return i;
   return -1;
};
void calculateTransactions(vector<Friend*>& f){
/* All the transaction calculations are done here.
To achieve minimal transaction count, first- the friend with the smallest expenses pay the difference between his expenses and average expenses.
Then, by resorting vector, the next friend with lowest expenses do the same. In the end everyone has equal expenses.
Side note: when he average sum is not equally dividable, some of the friends will pay a cent more than the average value.*/
   cout << "\nExpenses:\n";
   double total = 0;                               // total expenses from the vacation
   unsigned int f_count = f.size();                // total friend count
   for (unsigned int i = 0; i < f.size(); i++) {
      cout << f[i]->name << ": " << f[i]->expenses << " $\n";
      total+=f[i]->expenses;
   }
   double average = 0.01*floor((total / f_count)*100);   // average sum of expenses
   cout << "\nTotal: " << fixed << setprecision(2) <<total << " $\n";
   cout << "Average: " << fixed << setprecision(2) << average << " to be paid by mate.\n\n";
   sort(f.begin(), f.end(),compByExp); // sort friends in order from friend with the lowest expenses to one with the highest
   double b1;                                // technical variable used for calculations
   int diff = int(total*100) % f_count;
   /* in cases, when there cannot be equal expenses (total expenses callot be divided in equal parts), some friends will pay 1 cent more than others */
   double sum_to_pay;
   if (diff > 0) sum_to_pay = average + 0.01;
   else sum_to_pay = average;
   /* transaction using algorithm, that is described below function header */
   cout << "Transactions to be made: \n";
   for (unsigned int i = 0; fabs(f[0]->expenses - sum_to_pay) > 0.01; i++){
      b1 = f[0]->expenses - sum_to_pay;
      if ((-1)*b1 <= 0.01) break;
      f[0]->expenses -= b1;
      f[f_count-1]->expenses += b1;
      if (--diff == 0) sum_to_pay = average;
      cout << f[0]->name << " -> " << f[f_count-1]->name << " " << b1*(-1) << endl;
      sort(f.begin(), f.end(),compByExp);
   }
   // prints out friends and their expenses after the transactions have been finished
   cout << "\nResult:\n";
   for (unsigned int i = 0; i < f.size(); i++) {
      cout << f[i]->name << ": " << f[i]->expenses << " $\n";
   }
   return;
};
